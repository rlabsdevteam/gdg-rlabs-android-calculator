package org.rlabs.gdg.calculator;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class Main extends Activity implements OnClickListener {
	// private variable is a safe place to keep data while you application is
	// running.

	// the total below starts off with zero '0', but during the application it
	// changes so we storing whatever happens to the total here.
	private int total = 0;
	// Since we going to be changing the Result to display on the screen we
	// keeping the TextView declare here.
	private TextView textView1 = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// Info: At a start of any Android Application is the 'OnCreate'
		// function. This is where apps begins their execution.
		super.onCreate(savedInstanceState);
		// You can to let it load a view
		setContentView(R.layout.main);
		// Info: We create our first function to do more code for us
		setupView();
	}

	private void setupView() {
		// This button button links what is on your screen, the button you see
		// with something called a Listener.
		// A listener is a class in java that wait for something to happens, but
		// you got to ask it to listen to something first.
		Button add_button = (Button) findViewById(R.id.add_button);
		Button minus_button = (Button) findViewById(R.id.minus_button);
		// Here were asking the listener to listen for either the add button to
		// be pressed or the minus button to be pressed
		add_button.setOnClickListener(this);
		minus_button.setOnClickListener(this);

		// Since we declaring all elements on the XML, we will display the
		// result in the TextView.
		textView1 = (TextView) findViewById(R.id.textView1);
	}

	@Override
	public void onClick(View v) {
		// Since we got more than one button being pressed we need to identify
		// which button was pressed.
		// A 'switch' statement below help identify which button was pressed by
		// asking what its ID was
		switch (v.getId()) {
		case R.id.add_button:
			// Here all code relating to when a button is press comes in here,
			// until the word 'break'.
			// here we incrementing the total value by +1. By adding ++ after
			// the total is the same as total = total+1 -> 1 = 0 +1 -> 1
			total++;
			break;
		case R.id.minus_button:
			// here we decrementing the total value by -1. By adding -- after
			// the total is the same as total = total-1 -> 0 = 0 - 1 -> -1
			total--;
			break;
		}

		// To display the new total we need to set the TextView with the new
		// Total by pushing words into the setText function.
		textView1.setText("Result: " + total);
	}
}
